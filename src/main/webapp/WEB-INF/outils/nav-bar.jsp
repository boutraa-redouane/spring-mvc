<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Produit</title>

</head>
<body>
	<div class="container">

		<nav class="navbar navbar-expand-lg navbar-light bg-light">
			<a class="navbar-brand" href="/Spring-mvc/">Prod</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse"
				data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown"
				aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarNavDropdown">
				<ul class="navbar-nav">
					<li class="nav-item active"><a class="nav-link"
						href="/Spring-mvc/">Home<span class="sr-only">(current)</span></a>
					</li>
					<li class="nav-item dropdown"><a
						class="nav-link dropdown-toggle" href="#"
						id="navbarDropdownMenuLink" role="button" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false"> Product </a>
						<div class="dropdown-menu"
							aria-labelledby="navbarDropdownMenuLink">
							<a class="dropdown-item" href="/Spring-mvc/listPro">Liste</a>
							<sec:authorize access="isAuthenticated()">
								<a class="dropdown-item" href="/Spring-mvc/ajoutProduit">Ajout</a>

							</sec:authorize>
						</div></li>
					<sec:authorize access="isAuthenticated()">
						<li class="nav-item dropdown"><a
							class="nav-link dropdown-toggle" href="#"
							id="navbarDropdownMenuLink" role="button" data-toggle="dropdown"
							aria-haspopup="true" aria-expanded="false"> Categories </a>
							<div class="dropdown-menu"
								aria-labelledby="navbarDropdownMenuLink">
								<a class="dropdown-item" href="/Spring-mvc/listCat">Liste</a> <a
									class="dropdown-item" href="/Spring-mvc/ajoutCat">Ajout</a>

							</div></li>
					</sec:authorize>
					<li class="nav-item dropdown"><a
						class="nav-link dropdown-toggle" href="#"
						id="navbarDropdownMenuLink" role="button" data-toggle="dropdown"
						aria-haspopup="true" aria-expanded="false"> Users </a>
						<div class="dropdown-menu"
							aria-labelledby="navbarDropdownMenuLink">
							<sec:authorize access="!isAuthenticated()">
								<a class="dropdown-item" href="/Spring-mvc/log">Log in</a>
							</sec:authorize>
							<sec:authorize access="isAuthenticated()">


								<a class="dropdown-item" href="/Spring-mvc/logout">Log out</a>
								<a class="dropdown-item" href="/Spring-mvc/listUsers">liste</a>
								<a class="dropdown-item" href="/Spring-mvc/ajoutUsers">Add</a>
							</sec:authorize>
						</div></li>
				</ul>
			</div>
		</nav>