<%@include file="../outils/nav-bar.jsp"%>

<form method="post" action="ajoutProduit" enctype="multipart/form-data">
	<fieldset class="scheduler-border">
			<legend  class="scheduler-border">Ajout d'un produit</legend>
		<p>Vous pouvez ajouter un produit a partir de ce formulaire</p>

		<label class="form-check-label" for="label">label<span class="requis">*</span></label> 
		<input class="form-control" type="text" id="label" name="label" value="" size="20" maxlength="60" />
		<br /> 
		<label class="form-check-label" for="code">code<span class="requis">*</span></label> 
		<input class="form-control" type="integer" id="code" name="code" value="" size="20" maxlength="60" />
		<br /> 
		
		<label class="form-check-label" for="e">Prix<span class="requis">*</span></label> 
		<input class="form-control" type="integer" id="prix" name="prix" value="" size="20" maxlength="60" />
		<br /> <label for="quantite">Quantite<span class="requis">*</span></label>
		<input class="form-control" type="integer" id="quantite" name="quantite" value="" size="20" maxlength="20" /> <br /> 
		<label class="form-check-label" for="code-cat">Categorie<span class="requis">*</span></label> 
		
		<select class="form-control" name="categorie">
		<c:forEach items="${liste}" var="ele"> 
 			<option value="${ele.code}" >${ele.label}</option>
		</c:forEach>
		</select> 
		</br>
		<div class="custom-file">
		<label class="custom-file-label" for="image">image</label>
		<input class="custom-file-input" type="file" id="image" name="image" value="image" /> <br /> 
		</div>
		
		<input type="submit" value="Ajouter" class="btn btn-primary" /> <br />

	</fieldset>
</form>

<%@include file="../outils/footer.jsp"%>