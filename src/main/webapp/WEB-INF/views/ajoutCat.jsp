<%@include file="../outils/nav-bar.jsp" %>

<form method="post" action="ajoutCategorie">
		<fieldset class="scheduler-border">
			<legend  class="scheduler-border">Ajout d'une categorie</legend>
			<p>Vous pouvez ajouter une categorie a partir de ce formulaire</p>
			
			<label class="form-check-label" for="label">label<span class="requis">*</span></label> 
			<input class="form-control" type="text" id="label" name="label" value="" size="20" maxlength="60" /> <br /> 
			
			<label class="form-check-label" for="code">code<span class="requis">*</span></label> 
			<input class="form-control" type="text" id="code" name="code" value="" size="20" maxlength="60" /> <br /> 
	
			<input type="submit" value="Ajouter" class="btn btn-primary" /> <br />
		
		</fieldset>
	</form>

<%@include file="../outils/footer.jsp" %>