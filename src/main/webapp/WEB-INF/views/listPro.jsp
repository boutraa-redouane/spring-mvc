<%@include file="../outils/nav-bar.jsp"%>

<table class="table">
	<thead>
		<th>ID</th>
		<th>code Produit</th>
		<th>Label</th>
		<th>prix</th>
		<th>quantite</th>
		<th>image</th>
	</thead>

	<c:forEach items="${liste}" var="ele">
		<tr id="tr${ele.id}" style="">
			<td path="idProduit" method="get" id="idProduit"  >${ele.id}</td>
			<td >${ele.code}</td>
			<td>${ele.label}</td>
			<td>${ele.prix}</td>
			<td>${ele.quantite}</td>
			<td><div><img src="/Spring-mvc/image/${ele.image}" style=" width:25%; heigth:25px;"></div></td>

			<td style="" id="aff${ele.id}"><button
					onclick="afficherEditPro(${ele.id})">Editer</button></td>
			
			<td style="display: none" id="aff4${ele.id}"><input type="number"
				id="code${ele.id}" methode="GET" value="" maxlength="25"
				required="required" placeholder="code"></td>
					
			<td style="display: none" id="aff1${ele.id}"><input type="number"
				id="prix${ele.id}" methode="GET" value="" maxlength="25"
				required="required" placeholder="Prix"></td>
			
			<td style="display: none" id="aff3${ele.id}"><input type="number"
				id="qtt${ele.id}" methode="GET" value="" maxlength="25"
				required="required" placeholder="Quantite"></td>
			
			<td style="display: none" id="aff2${ele.id}" scope="row"><button>
					<a onclick="editProd(${ele.id})">valider</a>
				</button></td>
			
			<td scope="row"><button>
					<a onclick="delProd(${ele.id})">delete</a>
				</button></td>

		</tr>
	</c:forEach>

</table>
<script>
function afficherEditPro(id) {
	document.getElementById("aff1"+id).style.display = '';
	document.getElementById("aff2"+id).style.display = '';
	document.getElementById("aff3"+id).style.display = '';
	document.getElementById("aff4"+id).style.display = '';
	document.getElementById("aff"+id).style.display = 'none';
	
}
function validationprod(event) {
	event.stopPropagation();
	var retour = window
			.confirm("etes vous sur de vouloir supprimer cette personne ?");
	if (!retour) {
		event.preventDefault();
	}
}

function delProd(id) {
	console.log("Hello !")
	//var id = document.getElementById('code');
	console.log(id);
	retour = validationprod(event);
	if (!retour) {
		document.getElementById("tr"+id).style.display = 'none';
		reponse = $.ajax({
			url : "/Spring-mvc/deleteProd?id=" + id,
			type : "DELETE",
			dataType : "html",
			success : function(code_html, status) {
				
				a = JSON.parse(code_html);

				alert(a.msg + ", code : " + a.status);

			}
		});
		console.log(reponse);
	}
}

function validationeditprod(event) {
	event.stopPropagation();
	var retour = window
			.confirm("etes vous sur de vouloir modifier cette personne ?");
	if (!retour) {
		event.preventDefault();
	}
}

function editProd(id) {
	console.log("Hello !")
	var code = document.getElementById('code'+id).value;
	var prix = document.getElementById('prix'+id).value;
	var qtt = document.getElementById('qtt'+id).value;
	
	console.log(id);
	
	retour = validationeditprod(event);
	if (!retour) {
		
		reponse = $.ajax({
			url : "/Spring-mvc/editProd?id="+id +"&code=" + code+"&prix="+prix+"&qtt="+qtt,
			type : "GET",
			dataType : "html",
			success : function(code_html, status) {
				
				a = JSON.parse(code_html);

				alert(a.msg + ", code : " + a.status);
				location.reload(true)
			}
		});
		console.log(reponse);
	}
}
</script>
<%@include file="../outils/footer.jsp"%>
