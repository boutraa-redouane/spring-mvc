<%@include file="../outils/nav-bar.jsp"%>


<table class="table">
	<thead>
		<th scope="col">code Categorie</th>
		<th scope="col">Label</th>
	</thead>

	<c:forEach items="${liste}" var="ele">
		<tr id="tr${ele.code}" style="">
			
			<td path="code" methode="get" id="code" scope="row">${ele.code}</td>
			
			<td style="display: block; width: 50%" scope="row">${ele.label}</td>
			
			<td style="" id="aff${ele.code}"><button onclick="afficherEdit(${ele.code})">Editer</button></td>
			
			<td style="display: none" id="aff1${ele.code}"><input
				type="text" id="label${ele.code}" methode="GET" value=""
				maxlength="25" required="required" placeholder="boubidou"></td>
			
			<td style="display: none" id="aff2${ele.code}" scope="row"><button>
					<a onclick="edit(${ele.code})">valider</a>
				</button></td>
			<td scope="row"><button>
					<a onclick="del(${ele.code})">delete</a>
				</button></td>

		</tr>
	</c:forEach>

</table>



<script>
	function afficherEdit(id) {
		document.getElementById("aff1"+id).style.display = '';
		document.getElementById("aff2"+id).style.display = '';
		document.getElementById("aff"+id).style.display = 'none';
		
	}



	function validation(event) {
		event.stopPropagation();
		var retour = window
				.confirm("etes vous sur de vouloir supprimer cette personne ?");
		if (!retour) {
			event.preventDefault();
		}
	}

	function del(id) {
		console.log("Hello !")
		//var id = document.getElementById('code');
		console.log(id);
		retour = validation(event);
		if (!retour) {
			document.getElementById("tr"+id).style.display = 'none';
			reponse = $.ajax({
				url : "/Spring-mvc/delete?code=" + id,
				type : "DELETE",
				dataType : "html",
				success : function(code_html, status) {
					
					a = JSON.parse(code_html);

					alert(a.msg + ", code : " + a.status);

				}
			});
			console.log(reponse);
		}
	}
	
	function validationedit(event) {
		event.stopPropagation();
		var retour = window
				.confirm("etes vous sur de vouloir modifier cette personne ?");
		if (!retour) {
			event.preventDefault();
		}
	}

	function edit(id) {
		console.log("Hello !")
		var label = document.getElementById('label'+id).value;
		
		console.log(id);
		console.log(label)
		retour = validationedit(event);
		if (!retour) {
			
			reponse = $.ajax({
				url : "/Spring-mvc/edit?code=" + id+"&label="+label,
				type : "GET",
				dataType : "html",
				success : function(code_html, status) {
					
					a = JSON.parse(code_html);

					alert(a.msg + ", code : " + a.status);
					location.reload(true)
				}
			});
			console.log(reponse);
		}
	}
</script>
<%@include file="../outils/footer.jsp"%>
