<%@include file="../outils/nav-bar.jsp"%>

<table class="table">
	<thead>
		<th>Login</th>
		<th>Categorie</th>
		<th>Mot de passe</th>
		
	</thead>

	<c:forEach items="${liste}" var="ele">
		<tr>
			<td>${ele.login}</td>
			<td >${ele.cat}</td>
			<td >${ele.mdp}</td>
			
			<td scope="row"><button>
					<a onclick="delProd(${ele.id})">delete</a>
				</button></td>

		</tr>
	</c:forEach>

</table>
<script>
function afficherEditPro(id) {
	document.getElementById("aff1"+id).style.display = '';
	document.getElementById("aff2"+id).style.display = '';
	document.getElementById("aff3"+id).style.display = '';
	document.getElementById("aff4"+id).style.display = '';
	document.getElementById("aff"+id).style.display = 'none';
	
}
function validationprod(event) {
	event.stopPropagation();
	var retour = window
			.confirm("etes vous sur de vouloir supprimer cette personne ?");
	if (!retour) {
		event.preventDefault();
	}
}

function delProd(id) {
	console.log("Hello !")
	//var id = document.getElementById('code');
	console.log(id);
	retour = validationprod(event);
	if (!retour) {
		document.getElementById("tr"+id).style.display = 'none';
		reponse = $.ajax({
			url : "/Spring-mvc/deleteUser?id=" + id,
			type : "DELETE",
			dataType : "html",
			success : function(code_html, status) {
				
				a = JSON.parse(code_html);

				alert(a.msg + ", code : " + a.status);

			}
		});
		console.log(reponse);
	}
}

function validationeditprod(event) {
	event.stopPropagation();
	var retour = window
			.confirm("etes vous sur de vouloir modifier cette personne ?");
	if (!retour) {
		event.preventDefault();
	}
}

function editProd(id) {
	console.log("Hello !")
	var code = document.getElementById('code'+id).value;
	var prix = document.getElementById('prix'+id).value;
	var qtt = document.getElementById('qtt'+id).value;
	
	console.log(id);
	
	retour = validationeditprod(event);
	if (!retour) {
		
		reponse = $.ajax({
			url : "/Spring-mvc/editUser?id="+id +"&code=" + code+"&prix="+prix+"&qtt="+qtt,
			type : "GET",
			dataType : "html",
			success : function(code_html, status) {
				
				a = JSON.parse(code_html);

				alert(a.msg + ", code : " + a.status);
				location.reload(true)
			}
		});
		console.log(reponse);
	}
}
</script>
<%@include file="../outils/footer.jsp"%>
