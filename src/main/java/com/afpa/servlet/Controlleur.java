package com.afpa.servlet;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;



@Controller
public class Controlleur {


	@RequestMapping(value= {"/", "/index", "index.jsp"}, method = {RequestMethod.POST, RequestMethod.GET})
	public String index() {
		return "index";
	}
	
	@RequestMapping(value= {"image/**"}, method = RequestMethod.GET,  produces = MediaType.IMAGE_JPEG_VALUE)
	@ResponseBody
	public byte[] imgDownload( HttpServletRequest req, HttpServletResponse res, ModelAndView mv) throws IOException {
	
	String path = "F:\\Formation CDA\\TRAVAIL\\spring-mvc\\src\\main\\webapp\\image\\"+req.getRequestURI().split("/")[3];
	Path paths = Paths.get(path);
	
	 byte[] imgByte = Files.readAllBytes(paths);
	 
	 mv.addObject("leFile", imgByte);
	 
	return imgByte;
		 
	}
	
}
