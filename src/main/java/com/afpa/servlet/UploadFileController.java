package com.afpa.servlet;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

@Controller
@PropertySource("classpath:pathapplication.properties")
public class UploadFileController {
	
	@Value("${nom.fichier.path}")
	public String pathname;
	
	 // GET: Show upload form page.
	   @RequestMapping(value = "/uploadOneFile", method = RequestMethod.GET)
	   public ModelAndView uploadOneFileHandler(ModelAndView mv) {
		   System.out.println(pathname);
	      mv.setViewName("myUploadFormView");
	      return mv;
	   }
	   // POST: Do Upload
	   @RequestMapping(value = "/uploadOneFile", method = RequestMethod.POST)
	   public ModelAndView uploadOneFileHandlerPOST(HttpServletRequest request, ModelAndView mv, @RequestParam(value = "description", defaultValue = "none") String text,@RequestParam(value = "file")MultipartFile mFile ) {
	      mv.addObject("text", text);
	      mv.addObject("file", mFile);
		   return this.doUpload(request, mv);
	   }
	   private ModelAndView doUpload(HttpServletRequest request, ModelAndView mv) {
	      String description = (String) mv.getModel().get("text");
	      System.out.println("Description: " + description);
	      // Root Directory.
	      String uploadRootPath = pathname;
	      System.out.println("uploadRootPath=" + uploadRootPath);
	      File uploadRootDir = new  File(uploadRootPath);
	      // Create directory if it not exists.
	      if (!uploadRootDir.exists()) {
	         uploadRootDir.mkdirs();
	      }
	      MultipartFile fileData = (MultipartFile) mv.getModel().get("file");
	      //
	      List<File> uploadedFile = new ArrayList<File>();
	      List<String> failedFile = new ArrayList<String>();
	         // Client File Name
	         String name = fileData.getOriginalFilename();
	         System.out.println("Client File Name = " + name);
	         if (name != null && name.length() > 0) {
	            try {
	               // Create the file at server
	            	 File serverFile = new   File(pathname + File.separator + name);
	               BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(serverFile));
	               stream.write(fileData.getBytes());
	               stream.close();
	               //
	               uploadedFile.add(serverFile);
	               System.out.println("Write file: " + serverFile);
	            } catch (Exception e) {
	               System.out.println("Error Write file: " + name);
	               failedFile.add(name);
	            }
	         }
	      mv.addObject("description", description);
	      mv.addObject("uploadedFile", uploadedFile);
	      mv.addObject("failedFile", failedFile);
	      mv.setViewName("filUploadView");
	      return mv;
	   }
}