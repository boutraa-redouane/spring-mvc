package com.afpa.servlet;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.afpa.dto.ReponseDto;
import com.afpa.dto.ReponseStatut;
import com.afpa.dto.UsersDto;
import com.afpa.service.IUsersService;


@Controller
public class ControlleurUsers {

	@Autowired
	private IUsersService userService;

	@RequestMapping(value="/log", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView index(HttpServletRequest request, ModelAndView mv ) {
		HttpSession session = request.getSession();
		if(session.getAttribute("login") != null) {
			mv.addObject("Response", ReponseDto.builder().msg("vous etes deja connecté").status(ReponseStatut.KO).build());
			mv.setViewName("index");
			return mv;
		}else {
			mv.setViewName("onlog");
			return mv;
		}
	}
	
	@RequestMapping(value="/ajoutUsers", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView addUser(HttpServletRequest request, ModelAndView mv ) {
			mv.setViewName("ajoutUsers");
			return mv;
	}
	
	@RequestMapping(value="/ajoutUsers", method = RequestMethod.POST)
	@ResponseBody
	public ModelAndView addUserPost( ModelAndView mv, @RequestParam(name = "login") String login, @RequestParam(name="mdp") String code, 
			@RequestParam(name="cat", required = false) String cat ) {
			BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
			userService.add(UsersDto.builder().cat(cat).login(login).mdp(encoder.encode(code)).build());
			mv.setViewName("ajoutUsers");
			return mv;
	}
	
	@RequestMapping(value="/listUsers", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView suppr( ModelAndView mv) {
			List<UsersDto> liste = userService.findAll();
			
			mv.addObject("liste", liste);
			mv.setViewName("listUsers");
			return mv;
	}
	
	@RequestMapping(value="/logout", method = RequestMethod.GET)
	@ResponseBody
	public ModelAndView logout(HttpServletRequest request, ModelAndView mv ) {
		HttpSession session = request.getSession();
		session.invalidate();
		mv.setViewName("index");
		return mv;
	}


	@RequestMapping(value="/log", method = RequestMethod.POST)
	public ModelAndView redirectLog( ModelAndView mv, HttpSession session, @RequestParam(name = "mdp") String mdp, @RequestParam(name="login") String login) {
		System.err.println(session.getId().toString() + "      :  " + mdp + "      : " + login);
		
		int cat;
		
		if((cat = verificationUser(login, mdp)) != 0 ) {
			session.setAttribute("login", new Integer(cat));
			mv.addObject("login", new Integer(cat));
		}else {
			session.setAttribute("login", null);
			mv.addObject("login", null);
		}
		mv.setViewName("index");
		
		return mv;
	}
	
	private int verificationUser(String login, String mdp) {
		try {
			UsersDto user = userService.findByLogin(login);
			System.err.println(user.getLogin() + "   " + user.getMdp());
			if(login.compareTo(user.getLogin()) == 0 && mdp.compareTo(mdp) == 0) {
				return Integer.parseInt(user.getCat());
			}
			return 0;
		}catch (Exception e) {
			e.printStackTrace();
			return 0;
		}

	}

}
