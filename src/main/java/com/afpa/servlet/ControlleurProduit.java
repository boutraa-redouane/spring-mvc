package com.afpa.servlet;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.afpa.dto.CategorieDto;
import com.afpa.dto.ProduitDto;
import com.afpa.dto.ReponseDto;
import com.afpa.service.ICategorieService;
import com.afpa.service.IProduitService;

@Controller
public class ControlleurProduit {
	
	@Autowired
	private IProduitService prodService;
	
	@Autowired
	private ICategorieService catService;
	
	@RequestMapping(value="/ajoutProduit", method= RequestMethod.GET)
	public ModelAndView redirectAjoutProduit(ModelAndView mv) {
		List<CategorieDto> liste = catService.findAll();
		mv.setViewName("ajoutProduit");
		mv.addObject("liste", liste);
		
		return mv;
	}
	

	@RequestMapping(value="/ajoutProduit", method= RequestMethod.POST)
	public String ajoutProduit(@RequestParam(name = "label") String label, @RequestParam(name="categorie") int codeCat, 
		@RequestParam(name="prix") int prix, @RequestParam(name="quantite") int quantite, @RequestParam(name="code") int code,
		@RequestParam(value = "image")MultipartFile img, ModelAndView mv) {
		
		String chemin = "" + img.getOriginalFilename() +"."+ img.getContentType().split("/")[1];
		
		File dest = new File("F:\\Formation CDA\\TRAVAIL\\spring-mvc\\src\\main\\webapp\\image\\" + chemin); 
		try {
			img.transferTo(dest);
		} catch (IllegalStateException | IOException e) {
			
			e.printStackTrace();
		}
		
		prodService.addNew(ProduitDto.builder().label(label).image(chemin).cat(catService.findById(codeCat)).code(code).prix(prix).quantite(quantite).build());
		
//		mv.setViewName("listPro");
		return "redirect:listPro";
	}
	
	@RequestMapping(value="/listPro", method= RequestMethod.GET)
	public ModelAndView redirectListProduit(ModelAndView mv) {
		List<ProduitDto> liste = prodService.findAll("label", 5, 0);
		mv.setViewName("listPro");
		mv.addObject("liste", liste);
		return mv;
	}
	
	@RequestMapping(value="/deleteProd",method=RequestMethod.DELETE)
	@ResponseBody()
	public ReponseDto delete(@RequestParam(name="id") int idproduit ) {
		System.out.println(idproduit);
		return this.prodService.deleteByID(idproduit);
	}
	
	@RequestMapping(value = "/editProd",method = RequestMethod.GET)
	@ResponseBody
	public ReponseDto edit(@RequestParam(name = "id")int id,@RequestParam(name = "code")int code,@RequestParam(name="prix")double prix,@RequestParam(name = "qtt")int qtt) {
		System.out.println(id + "  " + code+ "" + prix + ""+qtt);
		return this.prodService.editbyID(id,code,prix,qtt);
	}
	
//	@RequestMapping(value="/ajoutProduit", method= RequestMethod.POST)
//	public String ajoutProduit(@PathVariable(name = "lab") String label) {
//		
//		prodService.addNew(ProduitDto.builder().cat(null).build());
//		return null;
//	}
	
	
}
