package com.afpa.servlet;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import com.afpa.dto.CategorieDto;
import com.afpa.dto.ReponseDto;
import com.afpa.service.ICategorieService;

@Controller
public class ControlleurCat {

	@Autowired
	private ICategorieService catService;
	
	
	@RequestMapping(value="/ajoutCat", method= RequestMethod.GET)
	public String directForm() {
		return "ajoutCat";
	}
	
	@RequestMapping(value="/ajoutCategorie", method= RequestMethod.POST)
	public ModelAndView ajoutCat(@RequestParam(name = "label") String label, @RequestParam(name="code") int code, ModelAndView mv) {
		catService.addNew(CategorieDto.builder().code(code).label(label).build());
		List<CategorieDto> liste = catService.findAll();
		mv.setViewName("listCat");
		mv.addObject("liste", liste);
		return mv;
	}
	
	@RequestMapping(value="/listCat")
	public ModelAndView redirectList(ModelAndView mv) {
		List<CategorieDto> liste = catService.findAll();
		mv.setViewName("listCat");
		mv.addObject("liste", liste);
		return mv;
	}
	
	
	@RequestMapping(value="/delete",method=RequestMethod.DELETE)
	@ResponseBody()
	public ReponseDto delete(@RequestParam(name="code") int code ) {
		return this.catService.deleteByID(code);
	}
	
	@RequestMapping(value = "/edit",method = RequestMethod.GET)
	@ResponseBody
	public ReponseDto edit(@RequestParam(name = "code")int code,@RequestParam(name = "label")String label) {
		System.out.println(code + "  " + label);
		return this.catService.editbyID(code, label);
	}
}
