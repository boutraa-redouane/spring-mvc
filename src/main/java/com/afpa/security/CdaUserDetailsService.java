package com.afpa.security;

import java.util.Objects;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.afpa.dao.UsersRepository;
import com.afpa.entity.Users;

@Service
public class CdaUserDetailsService implements UserDetailsService {
 
    @Autowired
    private final UsersRepository userDao;
    
    public CdaUserDetailsService(UsersRepository userDao) {
        this.userDao = userDao;
    }
 
    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Objects.requireNonNull(username);
        Users user = userDao.findByLogin(username);
        return new CdaUserPrincipal(user);
    }
}
