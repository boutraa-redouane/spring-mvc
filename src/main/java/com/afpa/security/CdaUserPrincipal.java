package com.afpa.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.afpa.entity.Users;

public class CdaUserPrincipal implements UserDetails {
	
	private static final long serialVersionUID = 1L;

	private Users user;
	 
    public CdaUserPrincipal(Users user) {
        this.user = user;
    }

	@Override
	public Collection<? extends GrantedAuthority> getAuthorities() {
		return null;
	}

	@Override
	public String getPassword() {
		System.err.println(this.user.getMdp());
		return this.user.getMdp();
	}

	@Override
	public String getUsername() {
		System.err.println(this.user.getLogin());
		return this.user.getLogin();
	}

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
}
