package com.afpa.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.afpa.entity.Categorie;

@Repository
public interface CategorieRepository extends CrudRepository<Categorie, Integer> {

	@Query("select e from Categorie e ")
	public List<Categorie> findAllCategorie	();
}
