 package com.afpa.dao;

import org.springframework.data.domain.Page;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.afpa.entity.Produit;

@Repository
public interface ProduitRepository extends PagingAndSortingRepository<Produit, Integer> {
	
	public Page<Produit> findAll();

}
