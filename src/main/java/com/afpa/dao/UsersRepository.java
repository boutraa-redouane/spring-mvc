package com.afpa.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.afpa.entity.Users;

@Repository
public interface UsersRepository extends CrudRepository<Users, Integer>{
	
	@Query("select e from Users e ")
	public List<Users> findAllUsers();
	
//	@Query("select e from Users e where e.login = :log")
	public Users findByLogin(String log);

}
