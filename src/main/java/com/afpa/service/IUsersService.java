package com.afpa.service;

import java.util.List;

import org.springframework.data.repository.query.Param;

import com.afpa.dto.UsersDto;

public interface IUsersService {

	public UsersDto findByLogin(@Param(value="log")String log);

	public boolean add(UsersDto build);

	public List<UsersDto> findAll();

}
