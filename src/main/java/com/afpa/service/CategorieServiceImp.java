package com.afpa.service;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.dao.CategorieRepository;
import com.afpa.dto.CategorieDto;
import com.afpa.dto.ReponseDto;
import com.afpa.dto.ReponseStatut;
import com.afpa.dto.ReponseDto.ReponseDtoBuilder;
import com.afpa.entity.Categorie;

@Service
public class CategorieServiceImp implements ICategorieService {

	@Autowired
	private CategorieRepository catDao;

	@Override
	public void addNew(CategorieDto cat) {

		catDao.save((new Categorie()).builder().code(cat.getCode()).label(cat.getLabel()).build());
		
	}

	@Override
	public List<CategorieDto> findAll() {
		return catDao.findAllCategorie().stream().map(x-> CategorieDto.builder().code(x.getCode()).label(x.getLabel()).build()).collect(Collectors.toList());
	}

	@Override
	public CategorieDto findById(int codeCat) {
		Optional<Categorie> c = catDao.findById(codeCat);
		if(c.isPresent()) {
			CategorieDto dto = CategorieDto.builder().code(c.get().getCode()).label(c.get().getLabel()).build();
			return dto;
		}
		return null;
	}

	@Override
	public ReponseDto deleteByID(int codecat) {
		ReponseDtoBuilder builder = ReponseDto.builder();
		if(this.catDao.existsById(codecat)) {
			this.catDao.deleteById(codecat);
			builder.status(ReponseStatut.OK);
			builder.msg("Suppression Effectué");
		} else {
			builder.status(ReponseStatut.KO);
			builder.msg("Suppression Echoue");
			
		}
		return builder.build();
	}

	@Override
	public ReponseDto editbyID(int codecat,String label) {
		ReponseDtoBuilder builder = ReponseDto.builder();
		if(this.catDao.existsById(codecat)) {
			Optional<Categorie> catMod = this.catDao.findById(codecat);
			if(catMod.isPresent()) {
				Categorie categorieDto = catMod.get();
				categorieDto.setLabel(label);
			this.catDao.save(categorieDto);
			builder.status(ReponseStatut.OK);
			builder.msg("Edition Effectué");
			}
			} else {
			builder.status(ReponseStatut.KO);
			builder.msg("Edition Echoue");
			
		}
		return builder.build();
	}
	
	
	
}
