package com.afpa.service;

import java.util.List;

import com.afpa.dto.ProduitDto;
import com.afpa.dto.ReponseDto;

public interface IProduitService {

	public void addNew(ProduitDto produitDto);

	public List<ProduitDto> findAll(String sortedBy, int nbrPage, int noPage);

	public ReponseDto deleteByID(int code);

	public ReponseDto editbyID(int id, int code, double prix, int qtt);

	
}
