package com.afpa.service;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.afpa.dao.ProduitRepository;
import com.afpa.dto.CategorieDto;
import com.afpa.dto.ProduitDto;
import com.afpa.dto.ReponseDto;
import com.afpa.dto.ReponseDto.ReponseDtoBuilder;
import com.afpa.dto.ReponseStatut;
import com.afpa.entity.Categorie;
import com.afpa.entity.Produit;

@Service
public class ProduitServiceImp implements IProduitService{

	@Autowired
	private ProduitRepository proDao;

	@Override
	public void addNew(ProduitDto pro) {
		Categorie c = Categorie.builder().code(pro.getCat().getCode()).label(pro.getCat().getLabel()).build();
		Produit p = Produit.builder().code(pro.getCode()).cat(c).image(pro.getImage()).label(pro.getLabel()).prix(pro.getPrix()).quantite(pro.getQuantite()).build();
		proDao.save(p);
	}

	@Override
	public List<ProduitDto> findAll( String p, int nbr, int no) {
		Pageable page = PageRequest.of(no, nbr, Sort.by(p));
		return proDao.findAll(page).stream().map(x -> {
			CategorieDto c = CategorieDto.builder().code(x.getCat().getCode()).label(x.getCat().getLabel()).build();
			 return ProduitDto.builder().id(x.getId()).cat(c).code(x.getCode()).image(x.getImage()).label(x.getLabel()).prix(x.getPrix()).quantite(x.getQuantite()).build();
		}).collect(Collectors.toList());
	}

	@Override
	public ReponseDto deleteByID(int code) {
		ReponseDtoBuilder builder = ReponseDto.builder();
		if(this.proDao.existsById(code)) {
			System.out.println(code);
			this.proDao.deleteById(code);
			builder.status(ReponseStatut.OK);
			builder.msg("Suppression Effectué");
		} else {
			builder.status(ReponseStatut.KO);
			builder.msg("Suppression Echoue");
			
		}
		return builder.build();
	
	}

	

    

	@Override
	public ReponseDto editbyID(int id, int code, double prix, int quantite) {
		ReponseDtoBuilder builder = ReponseDto.builder();
		if(this.proDao.existsById(id)) {
			Optional<Produit> proMod = this.proDao.findById(id);
			if(proMod.isPresent()) {
				Produit produitDto = proMod.get();
				produitDto.setCode(code);
				produitDto.setPrix(prix);
				produitDto.setQuantite(quantite);
				
			this.proDao.save(produitDto);
			builder.status(ReponseStatut.OK);
			builder.msg("Edition Effectué");
			}
			} else {
			builder.status(ReponseStatut.KO);
			builder.msg("Edition Echoue");
			
		}
		return builder.build();
	}
	
	
	
	
	
}
