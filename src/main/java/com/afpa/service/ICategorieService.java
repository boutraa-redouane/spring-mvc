package com.afpa.service;

import java.util.List;

import com.afpa.dto.CategorieDto;
import com.afpa.dto.ReponseDto;
import com.afpa.entity.Categorie;

public interface ICategorieService {

	public void addNew(CategorieDto build);

	public List<CategorieDto> findAll();

	public CategorieDto findById(int codeCat);
	
	public ReponseDto deleteByID(int codecat);

	public ReponseDto editbyID(int codecat,String label);
}
