package com.afpa.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.afpa.dao.UsersRepository;
import com.afpa.dto.UsersDto;
import com.afpa.entity.Users;

@Service
public class UsersServiceImp implements IUsersService {

	
	@Autowired
	private UsersRepository userDao;

	@Override
	public UsersDto findByLogin(String log) {
		Users user = userDao.findByLogin(log);
		
		return UsersDto.builder().login(user.getLogin()).mdp(user.getMdp()).cat(user.getCat()).build();
	}

	@Override
	public boolean add(UsersDto u) {
		userDao.save(Users.builder().cat(u.getCat()).login(u.getLogin()).mdp(u.getMdp()).build());
		
		return false;
	}

	@Override
	public List<UsersDto> findAll() {
		return userDao.findAllUsers().stream().map(x->UsersDto.builder().cat(x.getCat()).login(x.getLogin()).mdp(x.getMdp()).build()).collect(Collectors.toList());
	}
	
}
