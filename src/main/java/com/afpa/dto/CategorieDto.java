package com.afpa.dto;
	
import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class CategorieDto {

	private String label;
	private int code;
}
