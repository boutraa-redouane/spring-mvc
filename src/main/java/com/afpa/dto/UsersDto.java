package com.afpa.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder
public class UsersDto {

	private int id;
	private String login;
	private String mdp;
	private String cat;
}
