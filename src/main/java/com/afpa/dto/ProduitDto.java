package com.afpa.dto;

import lombok.Builder;
import lombok.Getter;

@Getter
@Builder	
public class ProduitDto {
	private int id;
	private int code;
	private String  label;
	private double prix;
	private int quantite;
	private String image;
	private CategorieDto cat;
}
